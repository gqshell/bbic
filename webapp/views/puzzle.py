from .views_common import *
import json


class PuzzleEntry:
    def __init__(self):
        # Current entry in the crossword.
        self.__val = None
        # Crossword numbering label. '-' signifies
        # this entry has no numbering. None signifies
        # this is a blocked out spot.
        self.__label = None
        # If set to True, mark this as a wrong answer
        self.__mark = None

    @property
    def val(self):
        return self.__val

    @val.setter
    def val(self, val):
        self.__val = str(val)

    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self, val):
        self.__label = str(val)

    @property
    def mark(self):
        return self.__mark

    @label.setter
    def mark(self, val):
        self.__mark = val


class PuzzleGrid:
    def __init__(self, rows, cols):
        self.__nrows = int(rows)
        self.__ncols = int(cols)
        # Row major packed 1D array
        self.__grid = [
            PuzzleEntry() for i in range(
                self.__nrows *
                self.__ncols)]

    @property
    def rows(self):
        return self.__nrows

    @property
    def cols(self):
        return self.__ncols

    def at_idx(self, idx):
        return self.__grid[idx]

    def at(self, irow, icol):
        idx = irow * self.cols + icol
        return self.at_idx(idx)


@app.route('/puzzle/<puzzle_id>')
def puzzle(puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    # URL decode puzzle_id
    puzzle_id = unquote_plus(puzzle_id)
    dim = get_dimensions(puzzle_id)
    puzzle = PuzzleGrid(dim[0], dim[1])
    solution = get_solution(puzzle_id)
    inputs = get_crossword_inputs(puzzle_id)
    # Default to normal if not specified
    mode = inputs['mode'] if 'mode' in inputs else 'normal'
    creator = inputs['creator']
    idx = 0
    clue = 1
    for c in solution:
        if c != ' ':
            num_cols = int(dim[1])
            x = idx % num_cols
            y = idx / num_cols
            if x == 0 or y == 0 or solution[idx -
                                            1] == ' ' or solution[idx -
                                                                  num_cols] == ' ':
                puzzle.at_idx(idx).label = clue
                clue += 1
            else:
                puzzle.at_idx(idx).label = '-'
            key = 'R' + str(y) + 'C' + str(x)
            if key in inputs:
                val = inputs[key]
                if len(val) == 1:
                    puzzle.at_idx(idx).val = val
        idx += 1
    return render_template(
        'puzzle.html',
        user=user,
        mode=mode,
        creator=creator,
        puzzle=puzzle,
        puzzle_id=puzzle_id,
        kloo_link=get_kloo_link(puzzle_id),
        other_users=list_users_for_puzzle(puzzle_id)
    )


@app.route('/puzzle_update/<puzzle_id>')
def puzzle_update(puzzle_id):
    user_login = request.args.get('login')
    entry = request.args.get('entry')
    val = request.args.get('value')
    print 'Updating puzzle {} entry {} for user {} to {}'.format(puzzle_id,
                                                                 entry,
                                                                 user_login,
                                                                 val)
    input_to_crossword(user_login, puzzle_id, entry, val)
    return 'OK'


def create_puzzle_from_inputs(dim, solution, inputs, mark_wrong):
    puzzle = PuzzleGrid(dim[0], dim[1])
    idx = 0
    clue = 1
    for c in solution:
        if c != ' ':
            num_cols = int(dim[1])
            x = idx % num_cols
            y = idx / num_cols
            if x == 0 or y == 0 or solution[idx -
                                            1] == ' ' or solution[idx -
                                                                  num_cols] == ' ':
                puzzle.at_idx(idx).label = clue
                clue += 1
            else:
                puzzle.at_idx(idx).label = '-'
            key = 'R' + str(y) + 'C' + str(x)
            if key in inputs and len(inputs[key]) == 1:
                puzzle.at_idx(idx).val = inputs[key]
                if mark_wrong and inputs[key] != c:
                    puzzle.at_idx(idx).mark = True
        idx += 1
    return puzzle


@app.route('/puzzle_check/<puzzle_id>')
def puzzle_check(puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    # URL decode puzzle_id
    puzzle_id = unquote_plus(puzzle_id)
    dim = get_dimensions(puzzle_id)
    solution = get_solution(puzzle_id)
    inputs = get_crossword_inputs(puzzle_id)

    puzzle = create_puzzle_from_inputs(dim, solution, inputs, True)

    return render_template(
        'puzzle_check.html',
        user=user,
        puzzle=puzzle,
        puzzle_id=puzzle_id,
        show_stats=False,
        other_users=list_users_for_puzzle(puzzle_id))


@app.route('/puzzle_view_and_check/<user_id>/<puzzle_id>')
def puzzle_view_and_check(user_id, puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    # URL decode puzzle_id
    puzzle_id = unquote_plus(puzzle_id)
    dim = get_dimensions(puzzle_id)
    solution = get_solution(puzzle_id)
    user_inputs = get_user_crossword_inputs(puzzle_id, user_id)

    puzzle = create_puzzle_from_inputs(dim, solution, user_inputs, True)
    return render_template(
        'puzzle_check.html',
        user=user,
        puzzle=puzzle,
        puzzle_id=puzzle_id,
        show_stats=False
    )


@app.route('/puzzle_view/<user_id>/<puzzle_id>')
def puzzle_view(user_id, puzzle_id):
    puzzle_id = unquote_plus(puzzle_id)
    user_id = unquote_plus(user_id)

    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    dim = get_dimensions(puzzle_id)
    sol = get_solution(puzzle_id)
    user_inputs = get_user_crossword_inputs(puzzle_id, user_id)

    puzzle = create_puzzle_from_inputs(dim, sol, user_inputs, False)
    return render_template(
        'puzzle_view.html',
        user=user,
        user_id=user_id,
        puzzle=puzzle,
        puzzle_id=puzzle_id
    )


def sol_to_puzzle(dim, sol):
    puzzle = PuzzleGrid(dim[0], dim[1])
    for idx, entry in enumerate(sol):
        entry = entry.strip()
        if entry:
            puzzle.at_idx(idx).val = entry
    return puzzle


def collect_user_stats(solution_puzzle, user_inputs, stats_table):
    username = user_inputs['login']
    stats_table['total-submissions-chart'][username] = 0
    stats_table['correct-submissions-chart'][username] = 0
    stats_table['incorrect-submissions-chart'][username] = 0
    for irow in range(solution_puzzle.rows):
        for icol in range(solution_puzzle.cols):
            key = 'R{}C{}'.format(irow, icol)
            if key not in user_inputs or user_inputs[key] == '--':
                continue
            stats_table['total-submissions-chart'][username] += 1
            correct = solution_puzzle.at(irow, icol).val
            if correct == str(user_inputs[key]):
                stats_table['correct-submissions-chart'][username] += 1
            else:
                stats_table['incorrect-submissions-chart'][username] += 1
    return


def generate_c3_config_json(chart_name, data):
    unsorted_column_data = [[user, data[user]] for user in data]
    '''
    sorted_column_data = sorted(
        unsorted_column_data,
        key=lambda k: k[1],
        reverse=True)
    '''
    # Don't sort which ensures the same color is used
    # to display the charts for a  particular user
    sorted_column_data = unsorted_column_data
    config = {
        'bindto': '#{}'.format(chart_name),
        'data': {
            'columns': sorted_column_data,
            'type': 'bar'
        },
        'bar': {
            'width': 30,
            'title': 'Total Submissions'
        }
    }
    return json.dumps(config, indent=2, sort_keys=True)


def prepare_cummulative_stats(puzzle_id, dim, solution):
    all_user_inputs = get_all_user_inputs(puzzle_id)
    solution_puzzle = sol_to_puzzle(dim, solution)
    stats_table = {
        # Total submission count by user
        'total-submissions-chart': {},
        # Correct count by user
        'correct-submissions-chart': {},
        # Incorrect count by user
        'incorrect-submissions-chart': {}
    }

    for user_inputs in all_user_inputs:
        collect_user_stats(solution_puzzle, user_inputs, stats_table)

    output_table = {}
    for itable in stats_table:
        output_table[itable] = generate_c3_config_json(
            itable, stats_table[itable])
    return output_table


@app.route('/puzzle_soln/<puzzle_id>')
def puzzle_soln(puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    # URL decode puzzle_id
    puzzle_id = unquote_plus(puzzle_id)
    dim = get_dimensions(puzzle_id)
    puzzle = PuzzleGrid(dim[0], dim[1])
    solution = get_solution(puzzle_id)
    inputs = get_crossword_inputs(puzzle_id)
    user_inputs = get_user_crossword_inputs(puzzle_id, user.login)
    c3_stats = prepare_cummulative_stats(puzzle_id, dim, solution)
    idx = 0
    clue = 1
    global_stats = [0, 0, 0, 0]  # correct, wrong, unattempted, total
    # correct, wrong, unattempted, correct but overridden, wrong but corrected
    user_stats = [0, 0, 0, 0, 0]
    for c in solution:
        if c != ' ':
            puzzle.at_idx(idx).val = c
            num_cols = int(dim[1])
            x = idx % num_cols
            y = idx / num_cols
            if x == 0 or y == 0 or solution[idx -
                                            1] == ' ' or solution[idx -
                                                                  num_cols] == ' ':
                puzzle.at_idx(idx).label = clue
                clue += 1
            else:
                puzzle.at_idx(idx).label = '-'
            key = 'R' + str(y) + 'C' + str(x)
            if key not in inputs or inputs[key] == '--':
                puzzle.at_idx(idx).mark = True
                global_stats[2] += 1
            elif inputs[key] != c:
                puzzle.at_idx(idx).mark = True
                global_stats[1] += 1
            else:
                global_stats[0] += 1
            # Individual stats
            if key not in user_inputs or user_inputs[key] == '--':
                user_stats[2] += 1
            elif user_inputs[key] != c and key in inputs and inputs[key] == c:
                user_stats[4] += 1
            elif user_inputs[key] != c:
                user_stats[1] += 1
            elif key not in inputs or inputs[key] != c:
                user_stats[3] += 1
            else:
                user_stats[0] += 1
        idx += 1
    global_stats[3] = global_stats[0] + global_stats[1] + global_stats[2]
    return render_template(
        'puzzle_check.html',
        user=user,
        puzzle=puzzle,
        puzzle_id=puzzle_id,
        show_stats=True,
        total_submissions_json=c3_stats['total-submissions-chart'],
        correct_submissions_json=c3_stats['correct-submissions-chart'],
        incorrect_submissions_json=c3_stats['incorrect-submissions-chart'],
        global_stats=global_stats,
        user_stats=user_stats,
        other_users=list_users_for_puzzle(puzzle_id))


@app.route('/puzzle_close/<puzzle_id>')
def puzzle_close(puzzle_id):
    close_crossword(puzzle_id)
    return redirect(url_for('home'))


@app.route('/puzzle_submit/<puzzle_id>')
def puzzle_submit(puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)

    # URL decode puzzle_id
    puzzle_id = unquote_plus(puzzle_id)
    dim = get_dimensions(puzzle_id)
    puzzle = PuzzleGrid(dim[0], dim[1])
    solution = get_solution(puzzle_id)
    inputs = get_crossword_inputs(puzzle_id)
    users = list_users_for_puzzle(puzzle_id)
    user_inputs = dict()
    for _user in users:
        user_inputs[_user] = get_user_crossword_inputs(puzzle_id, _user)
    c3_stats = prepare_cummulative_stats(puzzle_id, dim, solution)
    idx = 0
    clue = 1
    global_stats = [0, 0, 0, 0]  # correct, wrong, unattempted, total
    # correct, wrong, unattempted, correct but overridden, wrong but corrected
    user_stats = dict()
    for _user in users:
        user_stats[_user] = [0, 0, 0, 0, 0]
    for c in solution:
        if c != ' ':
            puzzle.at_idx(idx).val = c
            num_cols = int(dim[1])
            x = idx % num_cols
            y = idx / num_cols
            if x == 0 or y == 0 or solution[idx -
                                            1] == ' ' or solution[idx -
                                                                  num_cols] == ' ':
                puzzle.at_idx(idx).label = clue
                clue += 1
            else:
                puzzle.at_idx(idx).label = '-'
            key = 'R' + str(y) + 'C' + str(x)
            if key not in inputs or inputs[key] == '--':
                puzzle.at_idx(idx).mark = True
                global_stats[2] += 1
            elif inputs[key] != c:
                puzzle.at_idx(idx).mark = True
                global_stats[1] += 1
            else:
                global_stats[0] += 1
            # Individual stats
            for _user in users:
                if key not in user_inputs[_user] or user_inputs[_user][key] == '--':
                    user_stats[_user][2] += 1
                elif user_inputs[_user][key] != c and key in inputs and inputs[key] == c:
                    user_stats[_user][4] += 1
                elif user_inputs[_user][key] != c:
                    user_stats[_user][1] += 1
                elif key not in inputs or inputs[key] != c:
                    user_stats[_user][3] += 1
                else:
                    user_stats[_user][0] += 1
        idx += 1
    global_stats[3] = global_stats[0] + global_stats[1] + global_stats[2]
    for _user in users:
        add_user_stats(_user, puzzle_id, global_stats, user_stats[_user])
    close_crossword(puzzle_id)
    return render_template(
        'puzzle_check.html',
        user=user,
        puzzle=puzzle,
        puzzle_id=puzzle_id,
        show_stats=True,
        total_submissions_json=c3_stats['total-submissions-chart'],
        correct_submissions_json=c3_stats['correct-submissions-chart'],
        incorrect_submissions_json=c3_stats['incorrect-submissions-chart'],
        global_stats=global_stats,
        user_stats=user_stats[_user],
        other_users=list_users_for_puzzle(puzzle_id))
