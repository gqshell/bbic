from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
from flask_wtf.file import FileField, FileRequired
from wand.image import Image

import urllib2
import StringIO
import os
import sys

from .views_common import *


class ImageUploadForm(FlaskForm):
    name = StringField('Crossword Name (Date)', validators=[DataRequired()])
    url = StringField('Image URL', validators=[DataRequired()])


class KlooUploadForm(FlaskForm):
    name = StringField('Crossword Name (Date)', validators=[DataRequired()])
    kloofile = FileField('Kloo Image', validators=[FileRequired()])

# This could be a lambda, if we could package up tesseract


def perform_ocr(crossword_name, fname):
    s3 = appenv.boto3.resource('s3')
    dst = '/tmp/' + fname
    s3.meta.client.download_file('crossword-solutions', fname, dst)

    # Setup config file
    f = open('/tmp/tesseract_config', 'w')
    f.write('tessedit_char_whitelist ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    f.close()

    max_y = 15
    max_x = 15
    soln = ''
    for y in range(0, max_y):
        y_coord = int(11 + y * 26.6)
        for x in range(0, max_x):
            # Crop each letter of the 15x15 crossword
            x_coord = int(11 + x * 26.6)
            ofile = '/tmp/y' + str(y) + '_x' + str(x) + '.pgm'
            cmd = 'convert ' + dst + ' -crop 14x14+' + \
                str(x_coord) + '+' + str(y_coord) + ' ' + ofile
            os.system(cmd)

            cmd = 'convert -density 600 ' + ofile + ' -resize 64x64 ' + ofile
            os.system(cmd)

            # Get average Grayscale value to detect blanks
            avg_file = '/tmp/avg.txt'
            cmd = 'convert ' + ofile + \
                ' -colorspace Gray -format "%[mean]" info: > ' + avg_file
            os.system(cmd)
            with open(avg_file) as avg:
                floats = map(float, avg)
            if floats[0] < 500:
                soln += ' '
                continue

            # Run OCR
            cmd = 'tesseract ' + ofile + ' -psm 10 /tmp/out /tmp/tesseract_config'
            os.system(cmd)
            out = open('/tmp/out.txt')
            # I's don't get recognized by OCR, so assume unrecognized letters
            # are Is
            c = out.read(1)
            soln += c if c else 'I'

    d = dict()
    d['name'] = crossword_name
    d['num_rows'] = max_y
    d['num_cols'] = max_x
    d['data'] = soln
    set_solution(d)

    return soln


@app.route('/upload', methods=['GET'])
def upload():
    form = ImageUploadForm()
    return render_template('upload.html', form=form)


@app.route('/upload_file', methods=['POST'])
def upload_file():
    form = ImageUploadForm()
    if form.validate_on_submit():
        url = form.url.data
        filename = os.path.basename(url)
        fileobject = urllib2.urlopen(url)
        if not fileobject:
            return redirect(url_for('upload'))
        # Upload to S3
        fp = StringIO.StringIO(fileobject.read())
        s3 = appenv.boto3.resource('s3')
        bucket = s3.Bucket('crossword-solutions')
        bucket.put_object(Key=filename, Body=fp)

        return perform_ocr(form.name.data, filename)
    return redirect(url_for('upload'))


@app.route('/upload_kloo', methods=['GET', 'POST'])
def upload_kloo():
    form = KlooUploadForm()
    if form.validate_on_submit():
        file_data = form.kloofile.data
        puzzle_id = form.name.data
        s3_key = get_kloo_key(puzzle_id)
        s3 = appenv.boto3.resource('s3')
        bucket = s3.Bucket(get_kloo_s3_bucket())
        with Image(blob=file_data.stream) as img:
            img.format = 'jpg'
            bucket.put_object(
                Key=s3_key,
                Body=StringIO.StringIO(img.make_blob())
            )
        return 'Uploaded {}'.format(s3_key)
    else:
        return render_template('upload_kloo.html', form=form)
