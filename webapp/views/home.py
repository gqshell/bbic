from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired

from .views_common import *


@app.route('/home')
def home():
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)
    if user is None:
        # Stale cookie?
        return redirect(url_for('logout'))
    user_crosswords = set(list_crosswords(login))
    all_crosswords = set(list_crosswords())
    other_crosswords = all_crosswords - user_crosswords
    return render_template(
        'home.html',
        user=user,
        user_crosswords=user_crosswords,
        other_crosswords=other_crosswords)


class CreateCrosswordForm(FlaskForm):
    puzzle_name = StringField('User Name', validators=[DataRequired()])
    crossword = StringField('Crossword Name', validators=[DataRequired()])
    mode = StringField('Mode', validators=[DataRequired()])


@app.route('/create')
def create():
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)
    if user is None:
        # Stale cookie?
        return redirect(url_for('logout'))
    all_crosswords = list_crossword_templates()
    form = CreateCrosswordForm()
    return render_template(
        'create.html',
        user=user,
        crosswords=all_crosswords,
        form=form)


@app.route('/view_closed')
def view_closed():
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)
    if user is None:
        # Stale cookie?
        return redirect(url_for('logout'))
    all_crosswords = list_completed_crosswords(login)
    return render_template(
        'view_closed.html',
        user=user,
        crosswords=all_crosswords)


@app.route('/create_puzzle', methods=['POST'])
def create_puzzle():
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)
    if user is None:
        # Stale cookie?
        return redirect(url_for('logout'))
    form = CreateCrosswordForm()
    if form.validate_on_submit():
        # Create database entry
        full_name = form.puzzle_name.data + ' (' + form.crossword.data + ')'
        create_crossword(full_name, form.crossword.data, form.mode.data, login)
        return redirect(url_for('puzzle', puzzle_id=full_name))
    return redirect(url_for('create'))


@app.route('/join_puzzle/<puzzle_id>')
def join_puzzle(puzzle_id):
    if session.get('login') is None:
        return redirect(url_for('index'))
    login = session.get('login')
    user = get_user(login)
    if user is None:
        # Stale cookie?
        return redirect(url_for('logout'))
    puzzle_id = unquote_plus(puzzle_id)
    join_crossword(puzzle_id, login)
    return redirect(url_for('puzzle', puzzle_id=puzzle_id))
